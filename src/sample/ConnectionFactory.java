package sample;

import oracle.jdbc.pool.OracleDataSource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public final class ConnectionFactory {

    private static final String url = "jdbc:oracle:thin:@SCNDOSRV:1521:NDOORA11";
    private static final String username = "NDO";
    private static final String password = "NDO";


    public static Connection getConnection() throws SQLException {
        Properties prop = new Properties();
        prop.setProperty("MinLimit", "5");     // the cache size is 5 at least
        prop.setProperty("MaxLimit", "25");
        prop.setProperty("InitialLimit", "3"); // create 3 connections at startup
        prop.setProperty("InactivityTimeout", "1800");    //  seconds
        prop.setProperty("AbandonedConnectionTimeout", "900");  //  seconds
        prop.setProperty("MaxStatementsLimit", "10");
        prop.setProperty("PropertyCheckInterval", "60"); // seconds

        OracleDataSource source = new OracleDataSource();
        source.setUser(username);
        source.setPassword(password);
        source.setURL(url);
        source.setConnectionCacheProperties(prop);  // set properties
        return source.getConnection();
    }
}
