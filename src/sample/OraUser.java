package sample;

import java.time.LocalDateTime;
import java.util.Date;

public class OraUser {
    Integer id;
    String name;
    String direction;
    Date time;
    String address;

    public OraUser() {
    }

    public OraUser(Integer id, String name, String direction, Date time, String address) {
        this.id = id;
        this.name = name;
        this.direction = direction;
        this.time = time;
        this.address = address;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
