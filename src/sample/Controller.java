package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import java.sql.*;
import java.time.LocalDateTime;

public class Controller {
    public TableColumn colID;
    public TableColumn colNAME;
    public TableColumn colDIRECTION;
    public TableColumn colTIME;
    public TableColumn colADDRESS;
    public TextField txtSearch;
    public TableView tvOraObj;
    public RadioButton btAsc;
    public Label status;
    public Label numPage;
    public TextField pageSize;

    private static int currentRow = 1;
    private static int totalRow = 0;
    private static int selectedRow = 0;

    private final static String SQL_CNT = "select count(*) as cnt from USER_TABLE";
    private final static String SQL_MAIN = "select * from USER_TABLE %s order by name %s, rowid %s";

    private ObservableList<OraUser> listOraUser = FXCollections.observableArrayList();

    @FXML
    private void initialize() {
        initData("", true);

        // устанавливаем тип и значение которое должно хранится в колонке
        colID.setCellValueFactory(new PropertyValueFactory<OraUser, Integer>("id"));
        colNAME.setCellValueFactory(new PropertyValueFactory<OraUser, String>("name"));
        colDIRECTION.setCellValueFactory(new PropertyValueFactory<OraUser, String>("direction"));
        colTIME.setCellValueFactory(new PropertyValueFactory<OraUser, LocalDateTime>("time"));
        colADDRESS.setCellValueFactory(new PropertyValueFactory<OraUser, String>("address"));

        // заполняем таблицу данными
        tvOraObj.setItems(listOraUser);
    }

    private void initData(String searchStr, boolean isSortAsc) {
        listOraUser.clear();

        String sortOrder = "";
        if (!isSortAsc) {
            sortOrder = " desc";
        }

        String cause = "";
        if (searchStr.length() != 0) {
            cause = " where upper(name) like '%" + searchStr.toUpperCase() + "%'";
        }

        PreparedStatement stmt = null;
        ResultSet rs = null;

        try (Connection connection = ConnectionFactory.getConnection()) {
//            https://blogs.oracle.com/oraclemagazine/on-rownum-and-limiting-results
            String sql =
                    "select c.* \n" +
                            " from  (select c.*, rownum as rnum\n" +
                            "      from (" + SQL_MAIN + ") c\n" +
                            "      where rownum <= ?) c \n" +
                            " where c.rnum >= ?\n";
            sql = String.format(sql, cause, sortOrder, sortOrder);

            stmt = connection.prepareStatement(sql);
            stmt.setInt(1, currentRow + getMaxPage() - 1);
            stmt.setInt(2, currentRow);

            rs = stmt.executeQuery();
            while (rs.next()) {
                listOraUser.add(
                        new OraUser(
                                rs.getInt("id"),
                                rs.getString("name"),
                                rs.getString("direction"),
                                rs.getTimestamp("time"),
                                rs.getString("address")));
            }

            stmt = connection.prepareStatement(SQL_CNT);
            rs = stmt.executeQuery();
            if (rs.next()) {
                totalRow = rs.getInt("cnt");
            }

            stmt = connection.prepareStatement(SQL_CNT + cause);
            rs = stmt.executeQuery();
            if (rs.next()) {
                selectedRow = rs.getInt("cnt");
            }

            drawStatus(totalRow, selectedRow);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                rs.close();
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void search(ActionEvent actionEvent) {
        currentRow = 1;
        initData(txtSearch.getText(), isSortAsc());
        drawNumPage();
    }

    public void sortAsc(ActionEvent actionEvent) {
        currentRow = 1;
        initData(txtSearch.getText(), true);
        drawNumPage();
    }

    public void sortDesc(ActionEvent actionEvent) {
        currentRow = 1;
        initData(txtSearch.getText(), false);
        drawNumPage();
    }

    public void nextPage(ActionEvent actionEvent) {
        if (currentRow + getMaxPage() < selectedRow) {
            currentRow = currentRow + getMaxPage();
            initData(txtSearch.getText(), isSortAsc());
            drawNumPage();
        }
    }

    public void prevPage(ActionEvent actionEvent) {
        if (currentRow - getMaxPage() >= 1) {
            currentRow = currentRow - getMaxPage();
            initData(txtSearch.getText(), isSortAsc());
            drawNumPage();
        }
    }

    public void newPageSize(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.ENTER) {
            currentRow = 1;
            initData(txtSearch.getText(), isSortAsc());
            drawNumPage();
        }
    }

    private void drawStatus(int total, int select) {
        status.setText(Integer.toString(select) + " / " + Integer.toString(total));
    }

    private void drawNumPage() {
        numPage.setText(Integer.toString(currentRow) + " - " + Integer.toString(currentRow + getMaxPage() - 1));
    }

    private boolean isSortAsc() {
        if (btAsc.isSelected()) {
            return true;
        } else return false;
    }

    private int getMaxPage() {
        int maxPage = 25;
        try {
            maxPage = Integer.parseInt(pageSize.getText());
        } catch (Exception e) {
        }
        return maxPage;
    }

    //    private final String SQL = "select * from Test_Java order by nameval";
//    private final String SQL = "select * from USER_TABLE where name like 'Ильюшенко%'";
//    private final String SQL_INSERT = "insert into TEST_JAVA (idval, nameval, age) values (?, ?, ?)";
//    private final String SQL_UPDATE = "update TEST_JAVA set age = ? where nameval = ?";
//    private final String SQL_DELETE = "delete from TEST_JAVA where nameval = ?";
//    public TextField textField;
//
//    //    private PreparedStatement stmt = null;
//    private PreparedStatement stmt = null;
//    private ResultSet rs = null;
//
//    private ObservableList<OraUser> listOraUser = FXCollections.observableArrayList();
//
//    @FXML
//    private TableView<OraUser> tvORAOBJ;
//
//    @FXML
//    private TableColumn<OraUser, Integer> colIDVAL;
//
//    @FXML
//    private TableColumn<OraUser, String> colNAMEVAL;
//
//    @FXML
//    private TableColumn<OraUser, Integer> colAGE;
//
//    @FXML
//    private TableColumn<OraUser, Date> colDATE;
//
//    // инициализируем форму данными
//    @FXML
//    private void initialize() {
////        initData();
//
//
////
////        // устанавливаем тип и значение которое должно хранится в колонке
////        colIDVAL.setCellValueFactory(new PropertyValueFactory<OraUser, Integer>("idVal"));
////        colNAMEVAL.setCellValueFactory(new PropertyValueFactory<OraUser, String>("nameVal"));
////        colAGE.setCellValueFactory(new PropertyValueFactory<OraUser, Integer>("age"));
////        colDATE.setCellValueFactory(new PropertyValueFactory<OraUser, Date>("date"));
////
////        // заполняем таблицу данными
////        tvORAOBJ.setItems(listOraUser);
//    }
//
//    // подготавливаем данные для таблицы
//    // вы можете получать их с базы данных
//    private void initData() {
//        try (Connection connection = ConnectionFactory.oraConnect()) {
//            listOraUser.clear();
//            stmt = connection.prepareStatement(SQL);
//            rs = stmt.executeQuery(SQL);
//            while (rs.next()) {
//                listOraUser.add(
//                        new OraUser(
//                                rs.getInt("idVal"),
//                                rs.getString("nameVal"),
//                                rs.getInt("age"),
//                                rs.getDate("dtcreate")));
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
//
//    public void refresh(ActionEvent actionEvent) {
////        initData();
//        try {
//            // jdbc:oracle:thin:@SCNDOSRV:1521:NDOORA11
//            Properties prop = new Properties();
//            prop.setProperty("MinLimit", "5");     // the cache size is 5 at least
//            prop.setProperty("MaxLimit", "25");
//            prop.setProperty("InitialLimit", "3"); // create 3 connections at startup
//            prop.setProperty("InactivityTimeout", "1800");    //  seconds
//            prop.setProperty("AbandonedConnectionTimeout", "900");  //  seconds
//            prop.setProperty("MaxStatementsLimit", "10");
//            prop.setProperty("PropertyCheckInterval", "60"); // seconds
//
//            OracleDataSource source = new OracleDataSource();
//            source.setUser("NDO");
//            source.setPassword("NDO");
//            source.setURL("jdbc:oracle:thin:@SCNDOSRV:1521:NDOORA11");
//            source.setConnectionCacheProperties(prop);  // set properties
//
//            Connection connection = null;
//            try {
//                connection = source.getConnection();
//                System.out.println("Соединение установлено");
//                CallableStatement ca = connection.prepareCall("{? = call FULLNAMETOFAMIO(?)}");
//                ca.registerOutParameter(1, oracle.jdbc.OracleTypes.VARCHAR);
//                ca.setString(2,"Ильюшенко Евгений Николаевич");
////                stmt = connection.prepareStatement("select * from USER_TABLE where Upper(name) like ?");
////                stmt.setString(1,textField.getText().toUpperCase());
//                ca.execute();
////                sql иньекция
////                st = connection.prepareStatement(SQL);
////                rs = st.executeQuery("select * from USER_TABLE where Upper(name) like '%" + textField.getText().toUpperCase() + "%'");
//                System.out.println(ca.getString(1));
////                while (rs.next()) {
////                    System.out.println(rs.getString(1)
////
//////                            rs.getString("name") + "\t" +
//////                                    rs.getString("direction") + "\t" +
//////                                    rs.getDate("time") + "\t" +
//////                                    rs.getString("address")
////                    );
////                }
//            } catch (Exception exception) {
//                exception.printStackTrace();
//            } finally {
//                source.close();
//                connection.close();
//            }
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//
//    }
//
//    public void update(ActionEvent actionEvent) {
//        try (Connection connection = ConnectionFactory.oraConnect()) {
//
//            stmt = connection.prepareStatement(SQL_UPDATE);
//            stmt.setInt(1, 9999);
//            stmt.setString(2, "ien");
//            stmt.executeUpdate();
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        initData();
//
//        String aaa = "0000";
//        aaa = aaa.join("", "1111", "2222");
//    }
//
//    public void delete(ActionEvent actionEvent) {
//        try (Connection connection = ConnectionFactory.oraConnect()) {
//
//            stmt = connection.prepareStatement(SQL_DELETE);
//            stmt.setString(1, "ien");
//            stmt.executeUpdate();
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        initData();
//    }
//
//    public void insert(ActionEvent actionEvent) {
//        Connection connection = ConnectionFactory.oraConnect();
//        try {
//            connection.setAutoCommit(false);
//            stmt = connection.prepareStatement(SQL_INSERT);
//            stmt.setInt(1, 1234);
//            stmt.setString(2, "ien");
//            stmt.setInt(3, 54321);
//            stmt.execute();
//            connection.commit();
//        } catch (Exception ex) {
//            try {
//                connection.rollback();
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//            ex.printStackTrace();
//        }
//        initData();
//    }

}
